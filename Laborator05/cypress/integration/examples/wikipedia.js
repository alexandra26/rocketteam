describe('Scenario Test', () => {
    it("should render Wikipedia", () =>{
        var tData = require('./data.json');

        // select language
        cy.visit('https://www.wikipedia.org/');
        cy.contains('English').click();
        cy.url().should('include', 'Main_Page');

        // login
        cy.get("div").contains("Log in").click();
        cy.get('input[name = wpName]').type(tData.username);
        cy.get('input[name = wpPassword]').type(tData.password);
        cy.get('#wpLoginAttempt').click();

        // search UBB
        cy.get('input[name = search]').type(tData.search).type('{enter}');
        cy.wait(3000);
        cy.url().should("include", "UBB");

        // access Babes-Bolyai University
        cy.get("div").contains("Babeș-Bolyai University").click();
        cy.wait(3000);
        cy.url().should("include", "Bolyai");

        // logout
        cy.get("div").contains("Log out").click();
        cy.wait(3000);
        cy.url().should("include", "returnto=Babe");

        cy.clearCookies();
    })
});
