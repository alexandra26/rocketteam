describe('Login Test', () => {
    it("should login into Wikipedia", () =>{
        var tData = require('./data.json');

        cy.visit('https://en.wikipedia.org/wiki/Main_Page');
        cy.get("div").contains("Log in").click();
        cy.get('input[name = wpName]').type(tData.username);
        cy.get('input[name = wpPassword]').type(tData.password);
        cy.get('#wpLoginAttempt').click();

        cy.url().should("include", "Main");
        cy.clearCookies();
    });
});
