describe('Login Test', () => {
    it("should try login into Wikipedia with bad credentials", () => {
        var invalidData = require('./invalidData.json');

        cy.visit('https://en.wikipedia.org/wiki/Main_Page');
        cy.get("div").contains("Log in").click();
        cy.get('input[name = wpName]').type(invalidData.username);
        cy.get('input[name = wpPassword]').type(invalidData.password);
        cy.get('#wpLoginAttempt').click();

        cy.url().should('include', 'returnto=Main');
        cy.clearCookies();
    })
});
