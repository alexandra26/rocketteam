package pizzashop.service;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.*;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("PaymentValidatorTest")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PaymentValidatorTest {

    private static int tableNumber;
    private static PaymentType type;
    private static double amount;
    private static PizzaService pizzaService;

    @BeforeAll
    static void initPaymentValidator() {
        PaymentRepository paymentRepository = new PaymentRepository();
        MenuRepository menuRepository = new MenuRepository();
        pizzaService = new PizzaService(menuRepository, paymentRepository);
    }

    @BeforeEach
    void initDefaultParams() {
        type = PaymentType.Card;
        tableNumber = 2;
        amount = 100.0;
    }

    //BVA
    @Test
    @Order(1)
    @Tag("BVA")
    @Tag("TableNumber")
    @Tag("Exception")
    void testBadMinTableNumber() {
        tableNumber = 0;
        IllegalArgumentException ex = assertThrows(
                IllegalArgumentException.class,
                () -> pizzaService.addPayment(tableNumber, type, amount),
                "IllegalArgumentException expected!"
        );
        assertTrue(ex.getMessage().contains("Table number can't be lower than 1."));
    }

    @Test
    @Order(2)
    @Tag("BVA")
    @Tag("TableNumber")
    void testMinTableNumber() {
        tableNumber = 1;
        pizzaService.addPayment(tableNumber, type, amount);
    }

    @Test
    @Order(3)
    @Tag("BVA")
    @Tag("TableNumber")
    void testMaxTableNumber() {
        tableNumber = 8;
        pizzaService.addPayment(tableNumber, type, amount);
    }

    @Test
    @Order(4)
    @Tag("BVA")
    @Tag("Amount")
    @Tag("Exception")
    void testBadMinAmount() {
        amount = 0.0;
        IllegalArgumentException ex = assertThrows(
                IllegalArgumentException.class,
                () -> pizzaService.addPayment(tableNumber, type, amount),
                "IllegalArgumentException expected!"
        );
        assertTrue(ex.getMessage().contains("Amount can't be lower than 1.0."));
    }

    @Test
    @Order(5)
    @Tag("BVA")
    @Tag("Amount")
    void testGoodAmount() {
        amount = 2.0;
        pizzaService.addPayment(tableNumber, type, amount);
    }

    @Ignore
    @Disabled("Reason for disabling")
    @Test
    @Order(6)
    @Tag("BVA")
    @Tag("Amount")
    void testRightMinAmount() {
        amount = -2.0;
        pizzaService.addPayment(tableNumber, type, amount);
    }

    @Test
    @Order(7)
    @Tag("ECP")
    @Tag("PaymentType")
    void testType() {
        type = PaymentType.Cash;
        pizzaService.addPayment(tableNumber, type, amount);
    }


/*    @Test
    @Order(8)
    @Tag("ECP")
    @Tag("PaymentType")
    @Tag("Exception")
    void testBadType() {
        IllegalArgumentException ex = assertThrows(
                IllegalArgumentException.class,
                () -> paymentValidator.validatePayment(tableNumber, " ", amount),
                "IllegalArgumentException expected!"
        );
        assertTrue(ex.getMessage().contains("Type must be Cash or Card"));
    }*/


    @Test
    @Order(8)
    @Tag("ECP")
    @Tag("Amount")
    @Tag("Exception")
    void testBadAmount() {
        amount = -23.12;
        IllegalArgumentException ex = assertThrows(
                IllegalArgumentException.class,
                () -> pizzaService.addPayment(tableNumber, type, amount),
                "IllegalArgumentException expected!"
        );
        assertTrue(ex.getMessage().contains("Amount can't be lower than 1.0."));
    }

    /*@Test
    @Order(9)
    @Tag("ECP")
    @Tag("TableNumber")
    @Tag("Exception")
    void testBadTableNumber() {
        tableNumber = 9;
        IllegalArgumentException ex = assertThrows(
                IllegalArgumentException.class,
                () -> paymentValidator.validatePayment(" ", type, amount),
                "IllegalArgumentException expected!"
        );
        assertTrue(ex.getMessage().contains("Table number can't be higher than 8."));
    }
*/
    @Test
    @Order(9)
    @Tag("ECP")
    @Tag("TableNumber")
    @Tag("Exception")
    void testBadTableNumber() {
        tableNumber = 100;
        IllegalArgumentException ex = assertThrows(
                IllegalArgumentException.class,
                () -> pizzaService.addPayment(tableNumber, type, amount),
                "IllegalArgumentException expected!"
        );
        assertTrue(ex.getMessage().contains("Table number can't be higher than 8."));
    }

    @Test
    @Order(10)
    @Tag("ECP")
    @Tag("TableNumber")
    void testGoodTableNumber() {
        tableNumber = 2;
        pizzaService.addPayment(tableNumber, type, amount);
    }


}
