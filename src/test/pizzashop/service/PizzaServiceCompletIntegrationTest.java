package pizzashop.service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pizzashop.model.Payment;
import pizzashop.model.PaymentStub;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import pizzashop.service.PizzaService;

import java.util.List;

public class PizzaServiceCompletIntegrationTest {
    private static PaymentRepository paymentRepository;
    private static PizzaService pizzaService;

    @BeforeAll
    public static void setUp() {
        paymentRepository = new PaymentRepository();
        MenuRepository menuRepository = new MenuRepository();
        pizzaService = new PizzaService(menuRepository, paymentRepository);
    }

    @Test
    public void test_add_valid_payment() {
        Payment payment = new Payment(2, PaymentType.Card,23.4);
        paymentRepository.add(payment);
        List<Payment> payments= pizzaService.getPayments();
        assert(payments.equals(paymentRepository.getAll()));
    }

    @Test
    public void test_get_all_payments(){
        Payment payment = new Payment(2, PaymentType.Card,23.4);
        paymentRepository.add(payment);
        assert(paymentRepository.getPaymentList().equals(pizzaService.getPayments()));
    }
}
