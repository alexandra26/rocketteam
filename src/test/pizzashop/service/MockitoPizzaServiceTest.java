package pizzashop.service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.PaymentRepository;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("MockitoServiceTest")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MockitoPizzaServiceTest {

    @Mock
    private PaymentRepository repository;

    @InjectMocks
    private PizzaService service;

    @BeforeAll
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test_add_valid_payment() {
        Payment payment = new Payment(2, PaymentType.Card, 25.0);

        Mockito.when(repository.getAll()).thenReturn(Arrays.asList(payment));

        Mockito.verify(repository, Mockito.times(0)).add(payment);

        service.addPayment(payment.getTableNumber(), payment.getType(), payment.getAmount());

        assertEquals(1, service.getPayments().size());
    }

    @Test
    public void test_get_total_amount() {
        Payment payment1 = new Payment(2, PaymentType.Card, 25.0);
        Payment payment2 = new Payment(2, PaymentType.Card, 15.0);

        Mockito.when(repository.getAll()).thenReturn(Arrays.asList(payment1, payment2));

        double total = service.getTotalAmount(PaymentType.Card);

        assertEquals(40.0, total);
    }
}
