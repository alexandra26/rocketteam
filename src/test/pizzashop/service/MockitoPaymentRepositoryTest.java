package pizzashop.service;

import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.PaymentRepository;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("MockitoRepositoryTest")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MockitoPaymentRepositoryTest {

    @Mock
    private List<Payment> payments;

    @InjectMocks
    private PaymentRepository repository;

    @BeforeAll
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        payments.add(new Payment(2, PaymentType.Card, 25.0));
    }

    @Test
    @Order(1)
    public void test_add_payment() {
        Payment  payment= new Payment(2, PaymentType.Card, 15.0);

        Mockito.when(payments.size()).thenReturn(2);

        repository.add(payment);

        assertEquals(2, repository.getPaymentList().size());

    }

    @Test
    @Order(2)
    public void test_delete_payment() {
        Payment payment = new Payment(2, PaymentType.Card, 25.0);

        Mockito.when(payments.size()).thenReturn(0);

        repository.delete(payment);

        assertEquals(0, repository.getAll().size());


    }
}
