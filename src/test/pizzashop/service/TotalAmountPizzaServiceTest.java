package pizzashop.service;

import org.junit.jupiter.api.*;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("pizzashop.service.TotalAmountPizzaServiceTest")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TotalAmountPizzaServiceTest {

    private static PaymentType type;
    private static PizzaService pizzaService;
    private static List<Payment> list;

    @BeforeAll
    static void initPizzaService() {
        PaymentRepository paymentRepository = new PaymentRepository();
        MenuRepository menuRepository = new MenuRepository();
        pizzaService = new PizzaService(menuRepository, paymentRepository);
    }

    @Test
    @Order(1)
    void testTypeNullAndPaymentsListNull(){
        //set up
        type = null;
        list = null;

        //act
        double amount = pizzaService.getTotalAmountForPaymentsList(list,type);

        //assert
        assertTrue (amount == 0);
    }

    @Test
    @Order(2)
    void testNullPaymentsListTypeCash(){
        //set up
        type = PaymentType.Cash;
        list = null;

        //act
        double amount = pizzaService.getTotalAmountForPaymentsList(list,type);

        //assert
        assertTrue (amount == 0);
    }

    @Test
    @Order(3)
    void testNullPaymentsListTypeCard(){
        //set up
        type = PaymentType.Card;
        list = null;

        //act
        double amount = pizzaService.getTotalAmountForPaymentsList(list,type);

        //assert
        assertTrue (amount == 0);
    }


    @Test
    @Order(4)
    void testNullTypeEmptyList(){

        //setup
        type = null;
        list = new ArrayList<>();

        //act
        double amount = pizzaService.getTotalAmountForPaymentsList(list,type);

        //assert
        assertTrue (amount == 0);
    }

    @Test
    @Order(5)
    void testCardTypeEmptyList(){

        //setup
        type = PaymentType.Card;
        list = new ArrayList<>();

        //act
        double amount = pizzaService.getTotalAmountForPaymentsList(list,type);

        //assert
        assertTrue (amount == 0);
    }

    @Test
    @Order(6)
    void testCashTypeEmptyList(){

        //setup
        type = PaymentType.Cash;
        list = new ArrayList<>();

        //act
        double amount = pizzaService.getTotalAmountForPaymentsList(list,type);

        //assert
        assertTrue (amount == 0);
    }

    @Test
    @Order(7)
    void testCashTypeAllCardList(){

        //setup
        type = PaymentType.Cash;
        list = new ArrayList<>();
        list.add(new Payment(1,PaymentType.Card,3));
        list.add(new Payment(2,PaymentType.Card,4));

        //act
        double amount = pizzaService.getTotalAmountForPaymentsList(list,type);

        //assert
        assertTrue (amount == 0);

    }

    @Test
    @Order(8)
    void testCardTypeAllCashList(){

        //setup
        type = PaymentType.Card;
        list = new ArrayList<>();
        list.add(new Payment(1,PaymentType.Cash,3));
        list.add(new Payment(2,PaymentType.Cash,4));

        //act
        double amount = pizzaService.getTotalAmountForPaymentsList(list,type);

        //assert
        assertTrue (amount == 0);

    }

    @Test
    @Order(9)
    void testCashTypeGoodList(){

        //setup
        type = PaymentType.Cash;
        list = new ArrayList<>();
        list.add(new Payment(1,PaymentType.Cash,3));
        list.add(new Payment(2,PaymentType.Cash,4));
        list.add(new Payment(2,PaymentType.Card,4));

        //act
        double amount = pizzaService.getTotalAmountForPaymentsList(list,type);

        //assert
        assertTrue (amount == 7);

    }

    @Test
    @Order(10)
    void testCardTypeGoodList(){

        //setup
        type = PaymentType.Card;
        list = new ArrayList<>();
        list.add(new Payment(1,PaymentType.Cash,3));
        list.add(new Payment(2,PaymentType.Cash,4));
        list.add(new Payment(2,PaymentType.Card,4));

        //act
        double amount = pizzaService.getTotalAmountForPaymentsList(list,type);

        //assert
        assertTrue (amount == 4);

    }

    @Test
    @Order(11)
    void testNullTypeGoodList(){

        //setup
        type = null;
        list = new ArrayList<>();
        list.add(new Payment(1,PaymentType.Cash,3));
        list.add(new Payment(2,PaymentType.Cash,4));
        list.add(new Payment(2,PaymentType.Card,4));

        //act
        double amount = pizzaService.getTotalAmountForPaymentsList(list,type);

        //assert
        assertTrue (amount == 0);

    }





}
