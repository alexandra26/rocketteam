package pizzashop.service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import pizzashop.model.Payment;
import pizzashop.model.PaymentStub;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.util.List;

public class PaymentRepositoryIntegrationTest {

    private static PaymentRepository paymentRepository;
    private static PizzaService pizzaService;

    @BeforeAll
    public static void setUp() {
        paymentRepository = new PaymentRepository();
        MenuRepository menuRepository = new MenuRepository();
        pizzaService = new PizzaService(menuRepository, paymentRepository);
    }

    @Test
    public void test_add_valid_payment() {
        PaymentStub paymentStub = new PaymentStub();
        paymentRepository.add(paymentStub);
        List<Payment> payments= pizzaService.getPayments();
        assert(payments.equals(paymentRepository.getAll()));
    }

    @Test
    public void test_get_all_payments(){
        PaymentStub paymentStub = new PaymentStub();
        paymentRepository.add(paymentStub);
        assert(paymentRepository.getPaymentList().equals(pizzaService.getPayments()));
    }




}
