package pizzashop.model;

public class PaymentStub extends Payment {
    public PaymentStub() {
        super(2, PaymentType.Card, 100);
    }
}
