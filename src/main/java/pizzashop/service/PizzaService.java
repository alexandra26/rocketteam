package pizzashop.service;

import pizzashop.model.Menu;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.util.List;

public class PizzaService {

    private MenuRepository menuRepo;
    private PaymentRepository payRepo;
    private PaymentValidator paymentValidator;

    public PizzaService(MenuRepository menuRepo, PaymentRepository payRepo) {
        this.menuRepo = menuRepo;
        this.payRepo = payRepo;
        paymentValidator = new PaymentValidator();
    }

    public List<Menu> getMenuData() {
        return menuRepo.getMenu();
    }

    public List<Payment> getPayments() {
        return payRepo.getAll();
    }

    public void addPayment(int table, PaymentType type, double amount) {
        paymentValidator.validatePayment(table, type, amount);
        Payment payment = new Payment(table, type, amount);
        payRepo.add(payment);
    }

    public double getTotalAmount(PaymentType type) {
        List<Payment> l = getPayments();
        return getTotalAmountForPaymentsList(l, type);
    }

    public double getTotalAmountForPaymentsList(List<Payment> l, PaymentType type) {
        double total = 0.0f;
        if (l == null)
            return total;
        if (l.size() == 0)
            return total;
        for (Payment p : l) {
            if (p.getType().equals(type))
                total += p.getAmount();
        }
        return total;
    }

}
