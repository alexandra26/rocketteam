package pizzashop.service;

import pizzashop.model.PaymentType;

public class PaymentValidator {

    public void validatePayment(int tableNumber, PaymentType type, double amount) {
        if (tableNumber<1) throw new IllegalArgumentException("Table number can't be lower than 1.");
        if (tableNumber>8) throw new IllegalArgumentException("Table number can't be higher than 8.");
        if (amount < 1.0) throw new IllegalArgumentException("Amount can't be lower than 1.0.");
        if(!type.equals(PaymentType.Card)){
            if(!type.equals(PaymentType.Cash))
                throw new IllegalArgumentException("Type must be Cash or Card");
        }
    }
}
