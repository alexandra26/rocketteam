package pizzashop.utils;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import pizzashop.model.PaymentType;
import pizzashop.service.PizzaService;

import java.util.Optional;
import java.util.logging.Logger;

public class PaymentAlert implements PaymentOperation {
    private static final Logger LOGGER = Logger.getLogger(PaymentAlert.class.getName());
    private PizzaService service;
    private static String LINES = "--------------------------";

    public PaymentAlert(PizzaService service){
        this.service=service;
    }

    @Override
    public void cardPayment() {

        LOGGER.info(LINES);
        LOGGER.info("Paying by card...");
        LOGGER.info("Please insert your card!");
        LOGGER.info(LINES);
    }
    @Override
    public void cashPayment() {
        LOGGER.info(LINES);
        LOGGER.info("Paying cash...");
        LOGGER.info("Please show the cash...!");
        LOGGER.info(LINES);
    }
    @Override
    public void cancelPayment() {
        LOGGER.info(LINES);
        LOGGER.info("Payment choice needed...");
        LOGGER.info(LINES);
    }
      public void showPaymentAlert(int tableNumber, double totalAmount ) {
        Alert paymentAlert = new Alert(Alert.AlertType.CONFIRMATION);
        paymentAlert.setTitle("Payment for Table "+tableNumber);
        paymentAlert.setHeaderText("Total amount: " + totalAmount);
        paymentAlert.setContentText("Please choose payment option");
        ButtonType cardPayment = new ButtonType("Pay by Card");
        ButtonType cashPayment = new ButtonType("Pay Cash");
        ButtonType cancel = new ButtonType("Cancel");
        paymentAlert.getButtonTypes().setAll(cardPayment, cashPayment, cancel);
        Optional<ButtonType> result = paymentAlert.showAndWait();
        if (result.isPresent() && result.get() == cardPayment) {
            cardPayment();
            service.addPayment(tableNumber, PaymentType.Card,totalAmount);
        } else if (result.isPresent() && result.get() == cashPayment) {
            cashPayment();
            service.addPayment(tableNumber, PaymentType.Cash,totalAmount);
        } else if (result.isPresent() && result.get() == cancel) {
             cancelPayment();
        } else {
            cancelPayment();
        }
    }
}
